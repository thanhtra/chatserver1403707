/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatServer02;

import java.io.*;
import java.net.*;

/**
 *
 * @author admin
 */
public class Interpreter implements Runnable, ChatObserver {

    private ChatHistory history;
    private Socket clientSocket;
    private String username;
    private BufferedReader in;
    private PrintWriter out;

    public Interpreter(Socket s, ChatHistory h) {
        this.clientSocket = s;
        this.history = h;
        this.username = "Anonymous";
    }

    @Override
    public void run() {

        try {
            //Set up stream
            in = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));
            out = new PrintWriter(this.clientSocket.getOutputStream(), true);
            //Ask username
            System.out.println("New user coming . . .");
            //Register
            out.println("Give a username: ");
            if ((this.username = in.readLine()) != null) {

                //Greetings
                out.println("Hello " + this.username);
                out.println("You can start your chat \n");
            }
        } catch (IOException e) {
            System.out.println(e);
        }
        
        //print old chat entries
        for (String oldEntry : this.history.getEntries()) {
            out.println(oldEntry);
        }

        while (true) {
            try {
                String mess;
                if ((mess = in.readLine()) != null) {
                    String entry = this.username + ": " + mess;
                    this.history.getEntries().add(entry);
                    this.history.newEntryNotify(entry, this);
                }
            } catch (IOException e) {
                System.out.println(e);
            }
        }

    }

    @Override
    public void newEntry(String entry) {
        this.out.println(entry);
    }

}
