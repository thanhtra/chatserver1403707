/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatServer02;

import java.util.*;

/**
 *
 * @author admin
 */
public class ChatHistory {
    private static final ChatHistory INSTANCE = new ChatHistory();
    private List<String> entries;
    private List<ChatObserver> observers;
    
    
    private ChatHistory() {
        this.entries = new ArrayList<>();
        this.observers = new ArrayList<>();
    }

    public static ChatHistory getINSTANCE() {
        return INSTANCE;
    }

    public List<String> getEntries() {
        return entries;
    }

    public List<ChatObserver> getObservers() {
        return observers;
    }
    
    
    
    public void newEntryNotify(String entry, ChatObserver personAdd) {
        for (ChatObserver o : observers) {
            if (o != personAdd) {
                o.newEntry(entry);
            }
        }
    }
    
    
}
