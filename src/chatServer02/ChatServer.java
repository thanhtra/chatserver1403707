/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatServer02;

import java.io.*;
import java.net.*;
/**
 *
 * @author admin
 */
public class ChatServer {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        
        ChatHistory history = ChatHistory.getINSTANCE();
        ServerSocket ss = new ServerSocket(12345,50);
        System.out.println("Server running");
        
        while(true) {
            Socket clientSocket = ss.accept();
            Interpreter i = new Interpreter(clientSocket, history);
            history.getObservers().add(i);
            Thread t = new Thread(i);
            t.start();
        }
    }
    
}
